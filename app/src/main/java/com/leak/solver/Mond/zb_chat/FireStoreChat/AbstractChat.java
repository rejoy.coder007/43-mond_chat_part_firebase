package com.leak.solver.Mond.zb_chat.FireStoreChat;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

/**
 * Common interface for chat messages, helps share code between RTDB and Firestore examples.
 */
public abstract class AbstractChat {

    @Nullable
    public abstract Date getTimestamp();

    public abstract void setTimestamp(@Nullable Date timestamp);



    @Nullable
    public abstract String getTimeStampStr();

    public abstract void setTimeStampStr(String timeStampStr) ;


    @Nullable
    public abstract String getName();

    public abstract void setName(@Nullable String name);

    @Nullable
    public abstract String getMessage();

    public abstract void setMessage(@Nullable String message);

    @NonNull
    public abstract String getUid();

    public abstract void setUid(@NonNull String uid);

    @Override
    public abstract boolean equals(@Nullable Object obj);

    @Override
    public abstract int hashCode();

}
