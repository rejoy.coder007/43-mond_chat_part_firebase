package com.leak.solver.Mond.ab_home_page.aa_tabs.aa_ChatFragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.zz_Config;

public class SpaceShareFragment extends Fragment {


    public SpaceShareFragment() {
        // Required empty public constructor
    }


    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        if(zz_Config.TEST)
        {
            Log.d("_#_SPACE_FRAG_ON_CREATE", "FRAGMENT SPACE ON_CREATE   : aa_activity_home_screen__ "+mPage);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView=null;


        rootView = inflater.inflate(R.layout.ad_a_a_fragment_home_share_space, container, false);
       // TextView textView = (TextView) rootView.findViewById(R.id.share_space);
       // textView.setText(textView.getText()+" :: Fragment #" + mPage);


        FloatingActionButton fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                aa_activity_home_screen.mContext.fileManager_home.SendtoAllUsersActivity();
            }
        });


        if(zz_Config.TEST)
        {
            Log.d("_#_SPACE_FRAG__ON_VIEW", "FRAGMENT SPACE VIRECREATE   : aa_activity_home_screen__ "+mPage);

        }
         return rootView;
    }


    @Override
    public void onStop() {

        if(zz_Config.TEST)
        {
            Log.d("&_@@_#SPC_FRAG_STP", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::");

        }





        Runtime.getRuntime().gc();


        super.onStop();  // Always call the superclass method first

    }







}
