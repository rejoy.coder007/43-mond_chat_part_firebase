package com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview;
import android.os.Parcel;
import android.os.Parcelable;

import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;

public class paracelable_Folder_VideoPlayer implements Parcelable {

    public paracelable_Folder_VideoPlayer(int mImageResource, String mText1, String mText2, aa_activity_home_screen mContext, int currentIndex) {
        this.mImageResource = mImageResource;
        this.mText1 = mText1;
        this.mText2 = mText2;

        CurrentIndex = currentIndex;
        this.mContext = mContext;
    }

    private int mImageResource;
    private String mText1;
    private String mText2;
    public  aa_activity_home_screen mContext=null;

    public  int CurrentIndex=0;


    protected paracelable_Folder_VideoPlayer(Parcel in) {
        mImageResource = in.readInt();
        mText1 = in.readString();
        mText2 = in.readString();


        CurrentIndex = in.readInt();
    }

    public static final Creator<paracelable_Folder_VideoPlayer> CREATOR = new Creator<paracelable_Folder_VideoPlayer>() {
        @Override
        public paracelable_Folder_VideoPlayer createFromParcel(Parcel in) {
            return new paracelable_Folder_VideoPlayer(in);
        }

        @Override
        public paracelable_Folder_VideoPlayer[] newArray(int size) {
            return new paracelable_Folder_VideoPlayer[size];
        }
    };

    public int getImageResource() {
        return mImageResource;
    }

    public String getText1() {
        return mText1;
    }

    public String getText2() {
        return mText2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mImageResource);
        dest.writeString(mText1);
        dest.writeString(mText2);
        dest.writeInt(CurrentIndex);
    }

}
