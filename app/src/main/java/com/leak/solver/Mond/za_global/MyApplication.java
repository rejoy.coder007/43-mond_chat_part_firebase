package com.leak.solver.Mond.za_global;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

//import com.crashlytics.android.Crashlytics;
//import com.instabug.library.Instabug;
//import com.instabug.library.invocation.InstabugInvocationEvent;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.iid.FirebaseInstanceId;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zz_Config;
import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.DisplayLeakService;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.picasso.Picasso;

//import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.squareup.okhttp.OkHttpClient;
public class MyApplication extends Application   implements LifecycleObserver
{
    ListenerRegistration UsersList_registration_UserList;
    private FirebaseAuth mAuth=null;
    private FirebaseFirestore mFirestore;



    @Override
    public void onCreate()
    {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        mFirestore.setFirestoreSettings(settings);





        if(zz_Config.TEST)
        {
            Log.d("_#_ON _CREATE_APP", "onCreate: MyApplication");

        }



        if (LeakCanary.isInAnalyzerProcess(this))
        {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                .instanceField("android.view.inputmethod.InputMethodManager", "sInstance")
                .instanceField("android.view.inputmethod.InputMethodManager", "mLastSrvView")
                .instanceField("com.android.internal.policy.PhoneWindow$DecorView", "mContext")
                .instanceField("android.support.v7.widget.SearchView$SearchAutoComplete", "mContext")
                .build();


/*
        LeakCanary.refWatcher(this)
                .listenerServiceClass(DisplayLeakService.class)
              .excludedRefs(excludedRefs)
                .buildAndInstall();
*/
         LeakCanary.install(this);

        //LeakCanary.install(this);

        /*
        new Instabug.Builder(this, "46385c033cf94b747eb8c7dd4126e8c2")
                .setInvocationEvents(InstabugInvocationEvent.SHAKE, InstabugInvocationEvent.SCREENSHOT)
                .build();*/

      //  Fabric.with(this, new Crashlytics());

        // Normal app init code...

       // FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        /* Picasso */



        mAuth = FirebaseAuth.getInstance();







    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {

        if(zz_Config.CHAT_TEST)
        Log.d("MyApp", "App in background");
        if(mAuth.getCurrentUser() !=null)
        set_online_status(false);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        if(zz_Config.CHAT_TEST)
        Log.d("MyApp", "App in foreground");
        if(mAuth.getCurrentUser() !=null)
        set_online_status(true);
    }


    void set_online_status(Boolean online)
    {

        String token_id = FirebaseInstanceId.getInstance().getToken();
        String current_id = mAuth.getCurrentUser().getUid();

        HashMap<String, Object> UserStat = new HashMap<>();



        if(online){

            UserStat.put("online", true);

        }
        else
        {
            UserStat.put("online", false);

        }

        UserStat.put("time_stamp",  FieldValue.serverTimestamp());


        mFirestore.collection("UsersList").document(current_id).update(UserStat).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {



            }
        });



    }







/*

ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                    .instanceField("android.view.inputmethod.InputMethodManager", "sInstance")
                    .instanceField("android.view.inputmethod.InputMethodManager", "mLastSrvView")
                    .instanceField("com.android.internal.policy.PhoneWindow$DecorView", "mContext")
                    .instanceField("android.support.v7.widget.SearchView$SearchAutoComplete", "mContext")
                    .build();

            LeakCanary.refWatcher(this)
                    .listenerServiceClass(DisplayLeakService.class)
                    .excludedRefs(excludedRefs)
                    .buildAndInstall();
 */







}

